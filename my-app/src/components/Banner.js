// import Button from 'react-bootstrap/Button';
// import Row from 'react-bootstrap/Row';
// import Col from 'react-bootstrap/Col';
import {Button, Row, Col, Card, Container} from 'react-bootstrap';
import { Link } from 'react-router-dom';
import Pic1 from '../assets/sample1.jpg'

export default function Banner({data}) {

	console.log(data)

	const { title } = data

	return(
                
        <Container className='d-flex text-center' fluid>
        <Row>
		<Col className="p-5">
              <h2>"Fragrance with high quality and long lasting scent from different inspirations for all occassions."</h2>
             
             <Link to='/login'>
             <Button variant="primary">View Products!</Button>
             </Link>
               
            </Col>
        </Row>
        </Container>
    ) 

   
}
