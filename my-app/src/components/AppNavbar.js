import {useState, Fragment,useContext} from 'react';
import {Link} from 'react-router-dom';
import UserContext from '../UserContext';
import {Navbar, Nav, NavDropdown, Container} from 'react-bootstrap'
import {Form, FormControl, Button} from "react-bootstrap";
import Logo from '../assets/scentsailogo.jpg';
import Fb from '../assets/fb.png';
import Ig from '../assets/ig.png';
import Twitter from '../assets/twitter.png';
import UserIcon from '../assets/user.png';
import Search from '../assets/search.png';
import Cart from '../assets/cart.png';

export default function AppNavBar(){
  
   return (
     <Container classname="m-0 p-0"  fluid>
      <Navbar className='m-2 p-0' expand="lg">

      <Container classname="m-0 p-0" fluid>
        
        <Navbar.Toggle aria-controls="navbarScroll" />
        <Navbar.Collapse id="navbarScroll">
          <Nav
            className="me-auto my-2 my-lg-0"
            style={{ maxHeight: '100px' }}
            navbarScroll
          >
            <Link to='https://www.facebook.com/ScentsaiFragrancy'>
              <img src={Fb} className="fb m-2" width={30} />
              </Link>

            <Link to='https://www.instagram.com/littlerosaliza/'>
              <img src={Ig} className="ig m-2" width={30} />
            </Link>

            <Link to='https://twitter.com/aprilizette'>
              <img src={Twitter} className="twitter m-2" width={30} />
            </Link>
        </Nav>

          <Form className="d-flex m-2">
            <FormControl
              type="search"
              placeholder="Search"
              className="me-2"
              aria-label="Search"
            />
            <img src={Search} className="search m-2" width={20} />
          </Form>   
          <Link to='/login'>
            <img src={UserIcon} className="userIcon m-2" width={30} />
          </Link>
          <Link to='/myorders'>
            <img src={Cart} className="cart m-2" width={30} />
          </Link>
        </Navbar.Collapse>
      </Container>
      </Navbar>

      <Navbar className='mx-0 px-0'>
        <div className='d-flex justify-content-center w-100'>
        <Link to='/home'>
        <img src={Logo} className="logo" width={400} height={100}/>
        </Link>
        </div>
      </Navbar>

      <Navbar className='mx-0 px-0' bg="warning" expand="lg">

    <Navbar.Toggle aria-controls="basic-navbar-nav" />
    <Navbar.Collapse id="basic-navbar-nav">
            
    <Nav className='d-flex justify-content-center w-100 mx-0 px-0'>
      <Nav.Link>About</Nav.Link>

        <NavDropdown title="Fragrance" id="basic-nav-dropdown">
        <NavDropdown.Item>Fruity</NavDropdown.Item>
        <NavDropdown.Item>Floral</NavDropdown.Item>
        <NavDropdown.Item>Spicy</NavDropdown.Item>
        <NavDropdown.Item>Woody</NavDropdown.Item>
      </NavDropdown> 

      <NavDropdown title="Bestsellers" id="basic-nav-dropdown">
        <NavDropdown.Item>For Him</NavDropdown.Item>
        <NavDropdown.Item>For Her</NavDropdown.Item>
        <NavDropdown.Divider />
        <NavDropdown.Item>Diffuser</NavDropdown.Item>
      </NavDropdown>

      <Nav.Link>Gift Sets</Nav.Link>
      <Nav.Link>Join Us</Nav.Link>
      <Nav.Link>Contact Us</Nav.Link>
          
      </Nav>
    </Navbar.Collapse>
    </Navbar>
     </Container>
    
)
}


