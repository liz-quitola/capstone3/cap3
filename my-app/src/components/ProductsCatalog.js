import {Button, Row, Col, Card, Container} from 'react-bootstrap';
import { Link } from 'react-router-dom';
// import Pic1 from '../assets/sample1.jpg'
import Mens from '../assets/sample3.jpeg'
import Womens from '../assets/sample5.jpeg'
import Diffusers from '../assets/diffusers.jpg'
import MensCollection from '../pages/MensCollection';
import WomensCollection from '../pages/WomensCollection';
import DiffusersCollection from '../pages/DiffusersCollection';

export default function ProductsCatalog() {

        const {title} = "Fragrance with high quality and long lasting scent from different inspirations for all occassions."

	return(
                
        <Container className='d-md-flex justify-content-center' fluid>
        
		<Row>

        

    	    <Col className=" text-center p-5">
			    <Card style={{ width: '18rem' }}>
                <Card.Img variant="top" src={Mens} />
                <Card.Body>
                    <Card.Title>Men's Perfume</Card.Title>
                    <Card.Text>
                    Some quick example text to build on the card title and make up the bulk of
                    the card's content.
                    </Card.Text>
                    <Link className="btn btn-primary" to="/mensCollection">See Collection</Link>
                </Card.Body>
                </Card>
            </Col>
        </Row>
                 
       	<Col className="text-center p-5">
                <Card style={{ width: '18rem' }}>
                <Card.Img variant="top" src={Diffusers} />
                <Card.Body>
                    <Card.Title>Diffusers</Card.Title>
                    <Card.Text>
                    Some quick example text to build on the card title and make up the bulk of
                    the card's content.
                    </Card.Text>
                    <Link className="btn btn-primary" to="/diffusersCollection">See Collection</Link>
                </Card.Body>
                </Card>
               
        </Col>

        <Col className="text-center p-5">
                <Card style={{ width: '18rem' }}>
                <Card.Img variant="top" src={Womens} />
                <Card.Body>
                    <Card.Title>Women's Perfume</Card.Title>
                    <Card.Text>
                    Some quick example text to build on the card title and make up the bulk of
                    the card's content.
                    </Card.Text>
                    <Link className="btn btn-primary" to="/womensCollection">See Collection</Link>
                </Card.Body>
                </Card>
        </Col>
        
         
        </Container>
		)
}
