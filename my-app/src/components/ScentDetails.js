import { useEffect, useState } from 'react';
import {Row, Col, Card, Button} from 'react-bootstrap';
// import {useState, useEffect} from 'react';
import {Link} from 'react-router-dom';
import Swal from 'sweetalert2';






export default function ScentDetails ({scentsProp}) {
	// console.log(props)
	// console.log(typeof props)
	// console.log(courseProp)
	const {productName, inspiredBy, description,  size, price, _id} = scentsProp
    // const [count, setCount] = useState(0)
    // const [quantity, setquantity] = useState(30)

        // function addToCart(){

        //     setquantity(quantity - 1)
        //     setCount(count + 1)
        // }

        // useEffect(()=> {
        //     if (quantity === 0){
        //         Swal.fire({
		// 			title: "No more item on this scent left.",
		// 			icon: "error",
		// 			text: "You can choose other items."
		// 		})
        //     }
        // },[quantity]);

	return(
            <Row>
            <Col xs={12} md={12}>
            <Card>
					<Card.Body>
			    		<Card.Title>{productName}</Card.Title>
			    		<Card.Subtitle>
			    			Description:<br/>
			     			{description}
			     		</Card.Subtitle>
			    		<Card.Text className="mt-3">
			       			Price:<br/>
			       			{price}
			    		</Card.Text>
			    		{/*<Card.Text>Enrollees:{count}</Card.Text>
			    		<Card.Text>Seats available:{seat}</Card.Text>
			    		<Button variant="primary" onClick={enroll}>Enroll</Button>*/}
			    		<Link className="btn btn-primary" to={`/products/${_id}`}>Details</Link>
			    	</Card.Body>
				</Card>
            </Col>
            </Row>
		
		)
}