import {Row, Col, Card, Button, Container} from 'react-bootstrap';
// import {useState, useEffect} from 'react';
import {Link} from 'react-router-dom';


export default function ScentsCard({scentsProp}){

    const {productName, inspiredBy, category, description,  size, price, _id} = scentsProp


    return(

		<Container fliud>
        <Row className='d-block'>
			<Col className='w-50'>
			
				<Card className='justify-content-center'>
					<Card.Body className='text-center'>
			    		<Card.Title>{productName}</Card.Title>
                        <Card.Subtitle>
			    			Inspired By: {inspiredBy}
			     		</Card.Subtitle>
                         <Card.Text className='mt-2'>
			    			Category: {category}
			     		</Card.Text>
			    		<Card.Text>
			    			Size: {size}
			     		</Card.Text>
			    		<Card.Text className="mt-3">
			       			Price:<br/>
			       			{price}
			    		</Card.Text>
			    		<Link className="btn btn-primary" to="/details">Details</Link>
			    	</Card.Body>
				</Card>

				
				
			</Col>
		</Row>	
		</Container>

    )
}
