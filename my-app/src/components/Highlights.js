import {Container, Carousel} from 'react-bootstrap'
import Slide1 from '../assets/sample6.jpeg';
import Slide2 from '../assets/sample9.jpeg';
import Slide3 from '../assets/sample14.jpeg';

export default function Highlights (){

    return(
		<Container>
		<Carousel>
		<Carousel.Item>
			<img
			className="d-block w-100"
			src={Slide1}
			alt="First slide"
			/>
			<Carousel.Caption>
			<h3>Quality Fragrances</h3>
			<p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
			</Carousel.Caption>
		</Carousel.Item>
		<Carousel.Item>
			<img
			className="d-block w-100"
			src={Slide2}
			alt="Second slide"
			/>

			<Carousel.Caption>
			<h3>Long Lasting</h3>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
			</Carousel.Caption>
		</Carousel.Item>
		<Carousel.Item>
			<img
			className="d-block w-100"
			src={Slide3}
			alt="Third slide"
			/>

			<Carousel.Caption>
			<h3>Affordable</h3>
			<p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
			</Carousel.Caption>
		</Carousel.Item>
		</Carousel>
		</Container>
       
    )

}