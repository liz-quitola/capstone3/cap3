
import { useState, useEffect } from 'react';
import { UserProvider } from './UserContext';


import {Fragment} from 'react';
import {Container} from 'react-bootstrap';
import {BrowserRouter as Router} from 'react-router-dom';
import {Routes, Route} from 'react-router-dom';
import AppNavbar from './components/AppNavbar'; 
import Banner from './components/Banner';
import Highlights from './components/Highlights';
// import ScentsCard from './components/scentsCard';
import Login from './pages/Login';
import Logout from './pages/Logout'
import Register from './pages/Register';
import Home from './pages/Home';
import MensCollection from './pages/MensCollection';
import Products from './pages/Products';
import WomensCollection from './pages/WomensCollection';
import DiffusersCollection from './pages/DiffusersCollection';
import Details from './pages/Details';
import Cart from './pages/Cart';
 






import './App.css';

function App() {

  const [user, setUser] = useState({
  
   
    id: null,
    isAdmin: null
  })
  
    
    const unsetUser = () => {
      localStorage.clear();
    }
  
  
  useEffect(()=>{
   
  
  fetch("http://localhost:4000/users/details", {
    headers: {
      Authorization: `Bearer ${localStorage.getItem("token")}`
    }
  
  })
  .then(res => res.json())
  .then (data =>{
  
    if (typeof data._id !== "undefined") {
  
      setUser({
        id: data._id,
        isAdmin: data.isAdmin
      })
    } else {
      setUser({
        id: null,
        isAdmin: null
  
      })
       
    }
  
  })
  
  }, [])
  
  

  
  return (

<UserProvider value= {{user, setUser, unsetUser}}>
  <Router>
    <AppNavbar />
      <Container fluid>
    	  <Routes>
        <Route exact path="/" element={<div></div>} />
        <Route exact path="/login" element={<Login/>}/>
        <Route exact path="/logout" element={<Logout/>}/>
        <Route exact path="/register" element={<Register/>}/>
        <Route exact path="/home" element={<Home/>}/>
        <Route exact path="/mensCollection" element={<MensCollection/>}/>
        <Route exact path="/products" element={<Products/>}/>
        <Route exact path="/womensCollection" element={<WomensCollection/>}/>
        <Route exact path="/diffusersCollection" element={<DiffusersCollection/>}/>
        <Route exact path="/details" element={<Details/>}/>
        <Route exact path="/cart" element={<Cart/>}/>
        </Routes>
      </Container>

  </Router>
  </UserProvider>
 
  );
}

export default App;