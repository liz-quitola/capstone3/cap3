import PropTypes from 'prop-types';
import {Fragment, useState, useEffect} from 'react';
import ScentsCard from '../components/ScentsCard';


export default function WomensCollection (){

	const [womensScent, setWomensScent] = useState([]);

	useEffect(()=> {
		fetch("http://localhost:4000/scents/products")
		.then(res => res.json())
		.then(data =>{

			setWomensScent(data.filter(current => current.category == "For Her"));
			
		})

	},[])
	return(
		<Fragment>
			{womensScent.map(scent => {
				return<ScentsCard key={scent._id} scentsProp={scent}/>
			})}
		</Fragment>
	)
}