import PropTypes from 'prop-types';
import {Fragment, useState, useEffect} from 'react';
import ScentsCard from '../components/ScentsCard';


export default function DiffusersCollection (){

	const [diffusersScent, setDiffusersScent] = useState([]);

	useEffect(()=> {
		fetch("http://localhost:4000/scents/products")
		.then(res => res.json())
		.then(data =>{

			setDiffusersScent(data.filter(current => current.category == "Diffuser"));
			
		})

	},[])
	return(
		<Fragment>
			{diffusersScent.map(scent => {
				return<ScentsCard key={scent._id} scentsProp={scent}/>
			})}
		</Fragment>
	)
}