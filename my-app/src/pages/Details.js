import { useState, useEffect, useContext } from 'react'
import { Container, Card, Row, Col, Button } from 'react-bootstrap';
import {useParams, useNavigate, Link} from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';


export default function Details() {

	const {user} = useContext(UserContext);

	const navigate = useNavigate();

	// The "useParams" allows us to retrieve the courseId passed via the URL
	const {scentsId} = useParams()
	const [productName, setProductName] = useState("");
    const [productId, setProductId] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);

	const addToCart = (productId) => {

		fetch('http://localhost:4000/orders/users/checkout', {
			method:'POST',
			headers: {
				"Content-Type": "application/json",
				 Authorization: `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
					productId: productId
				})
			})

			.then (res => res.json())
			.then(data => {

			// console.log(data)
			if(data === true) {
				Swal.fire({
					title: "Successfully Added to Cart!",
					icon: "success",
					text: "You have successfully added this item to your cart."
				})

				navigate("/products")

			} else{
				Swal.fire({
					title: "Something went wrong!",
					icon: "error",
					text: "Please try again."
				})
			}
			

		})
	}


	useEffect(()=>{

		// console.log(courseId)
		fetch(`http://localhost:4000/scents/${productId}`)
		.then(res => res.json())
		.then (data => {

			// console.log(data)

			setProductName(data.productName);
			setDescription(data.description);
			setPrice(data.price)
		})

	}, [productId, description, price])

	return(

		<Container className="mt-5">
			<Row>
				<Col lg={{ span:6, offset: 3}}>
					<Card>
						<Card.Body className="text-center">
							<Card.Title>{productName}</Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>Php {price}</Card.Text>
							
							{

								(user.id !== null) ?
								<Button variant="primary" onClick={() => addToCart(productId)}>Add to Cart</Button>
								:
								<Link className="btn btn-danger" to="/login">Login to Enroll</Link>

							}
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
	)

}
