import { useState, useContext, useEffect } from 'react';
import {Link} from 'react-router-dom';
import {Navigate, useNavigate} from 'react-router-dom';
import {Form, Button, Container} from 'react-bootstrap';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function Login() {

	const {user, setUser} = useContext(UserContext);
	const [email, inputEmail] = useState('');
	const [password, inputPassword] = useState('');
	const [isActive, setIsActive] = useState(false);
	const navigate  = useNavigate(useNavigate);

	function userLogin(e) {
			
		e.preventDefault()
	
			
		fetch(`http://localhost:4000/users/login`, {
	
			method:'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then (res => res.json())
		.then(data => {
	
				console.log(data)
	
			if (typeof data.access !== "undefined") {

				localStorage.setItem('token', data.access)
				retrieveUserDetails(data.access)

				Swal.fire({
					title: "Login Successful",
					icon: "success",
					text: "Welcome to Scentsai Fragrancy!"
				})
				
				navigate('/products')

			} else{
				Swal.fire({
					title: "Authentication failed",
					icon: "error",
					text: "Check your login details and try again."
				})
			}
		})

		inputEmail('');
		inputPassword('');

		
		// alert("You are now logged in.")
	}

	const retrieveUserDetails = (token) => {

		fetch('http://localhost:4000/users/details', {
			headers:{
				Authorization: `Bearer ${token}`
			}
		})

		.then (res => res.json())
		.then(data => {

		// console.log(data)
		// console.log(data._id)
		// console.log(data.isAdmin)

		setUser({
			id: data._id,
			isAdmin: data.isAdmin
		})

	})

}

	useEffect(()=>{

		if(email !== '' && password !== '') {
			setIsActive(true)
			
		} else {
			setIsActive(false)
			
		}
	}, [email, password])

	return(

		(user.id !== null) ? 

		<Navigate to ="/products" />
		
		:
		
		<div >
		<Container className='d-flex justify-content-center'>
		<Form onSubmit = {(e) => userLogin(e)}>
			<h1>Login</h1>
	
			<Form.Group className="mb-3" controlId="userEmail">
			<Form.Label>Email Address</Form.Label>
			<Form.Control
				type="email" 
				placeholder="Enter email"
				value = {email}
				onChange = {e => inputEmail(e.target.value)} 
				required />
			</Form.Group>

			<Form.Group className="mb-3" controlId="password">
		    <Form.Label>Password</Form.Label>
		    <Form.Control 
		    	type="password" 
		    	placeholder="Password"
		    	value={password}
		    	onChange= {e => inputPassword(e.target.value)} 
		    	required/>
			</Form.Group>
	
				{
				isActive ?
				<Button variant="primary" type="submitBtn">
				Login
				</Button>
				:
				<Button variant="danger" type="submitBtn" disabled>
			Login
				</Button>
				}
		</Form>
		</Container>
			<div className='d-flex justify-content-center mt-5'>
				<p>Got no account yet? Register <Link to='/register'>Here</Link>.</p>
			</div>
		
		</div>
		
	)

}