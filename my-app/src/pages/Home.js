import { Fragment } from "react";
import Banner from "../components/Banner";
import Highlights from "../components/Highlights";
// import Scents from "../data/scentsData";

export default function Home() {

    const data = {      
       title: "Fragrance with high quality and long lasting scent from different inspirations for all occassions."
    }

    return (
    	
        <Fragment>
            <Highlights />
            <Banner data = {data} />
           
            {/* <CourseCard /> */}
        </Fragment>
    );
}