import PropTypes from 'prop-types';
import {Fragment, useState, useEffect} from 'react';
import ScentsCard from '../components/ScentsCard';


export default function MensCollection (){

	const [mensScent, setMensScent] = useState([]);

	useEffect(()=> {
		fetch("http://localhost:4000/scents/products")
		.then(res => res.json())
		.then(data =>{

			setMensScent(data.filter(current => current.category == "For Him"));
			
		})

	},[])
	return(
		<Fragment>
			{mensScent.map(scent => {
				return<ScentsCard key={scent._id} scentsProp={scent}/>
			})}
		</Fragment>
	)
}