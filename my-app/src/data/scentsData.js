const scentsData = [
    {
        id:"ACQUADIGIO",
        productName: "Acqua Di Gio",
        inspiredBy: "Giorgio Armani",
        category: "For Him",
        description: "Wonderful citrus, aquatic, spicy, musk smell that is well done by perfumer Alberto Morillas.",
        size: "85ml",
        price: 300,
        isActive: true
    },
    {
        id:"AVENTUS",
        productName: "Aventus",
        inspiredBy: "Creed",
        category: "For Him",
        description: "It is a very fresh scent that oozes masculinity and great success. If you smell Creed Aventus you immediately think success, power, luxury, and fame.",
        size: "85ml",
        price: 300,
        isActive: true
    },
    {
        id:"BLEU",
        productName: "Bleu de Chanel",
        inspiredBy: "Chanel",
        category: "For Him",
        description: "Bleu de Chanel is described as a woody aromatic fragrance, which is identified by the combination of aromatic herbs and an opulent center and base. The fragrance contains top notes of lemon, mint, pink pepper, and grapefruit; middle notes of ginger, Super, nutmeg, and jasmine; and base notes of labdanum, sandalwood, patchouli, vetiver, incense, cedar, and white musk",
        size: "85ml",
        price: 250,
        isActive: true
    },
    {
        id:"EXTREME",
        productName: "Bvlgari Extreme",
        inspiredBy: "Bvlgari",
        category: "For Him",
        description: "It has a masculine edge to it, but the dry down is where you'll get just a tiny bit of sweetness combined with the woods. It's a safe, versatile, workhorse that will get you average projection and longevity.",
        price: 250,
        isActive: true
    },
    {
        id:"COOLWATER",
        productName: "Cool Water",
        inspiredBy: "Davidoff",
        category: "For Him",
        description: "Both musky and refreshing ... A fresh, musky scent for men that's perfect for women, too. Initially you get a minty aquatic scent which lasts for about 3 hours. It slowly dies down around the 2-3 hour mark but is still noticeable. As time goes on the scent slightly transforms into a musky scent with slight mint.",
        size: "85ml",
        price: 250,
        isActiver: true
    },
    {
        id:"BOMBSHELL",
        productName: "Bombshell",
        inspireBy: "Victoria's Secret",
        category: "For Her",
        description: "The fruity-floral is a blend of citrus, Brazilian purple passion fruit, Madagascan vanilla orchid, and Italian pine, resulting in a bright, confident, sunshine-y fragrance.",
        size: "85ml",
        price: 250,
        isActive: true
    },
    {
        id:"BRIGHT",
        productName: "Bright Crystal",
        inspiredBy: "Versace",
        category: "For Her",
        description: "Fresh, sensual blend of refreshing chilled yuzu and pomegranate mingled with soothing blossoms of peony, magnolia, and lotus flower, warmed with notes of musk and amber.",
        size: "85ml",
        price: 300,
        onOffer: true
    },
    {
        id:"CHLOE",
        productName: "Chloe",
        inspiredBy: "Versace",
        category: "For Her",
        description: "Begins with a combination of floral powdery notes: hints of peony, lychee, and springtime freesia. The airy, flirtatious head notes drift away to reveal the richer and more sensual side of the rose.",
        size: "85ml",
        price: 250,
        isActive: true
    },
    {
        id:"ENGLISHPEAR",
        productName: "English Pear and Freesia",
        inspiredBy: "Jo Malone",
        category: "For Her",
        description: "Combines the “freshness of sweet pears, wrapped in a bouquet of white freesias, on a subtle background of scrambling wild roses and skin-warming amber, patchouli and woods.",
        size: "85ml",
        price: 250,
        isActive: true
    },
    {
        id:"AMETHYSTE",
        productName: "Omnia Amethyste",
        inspiredBy: "Bvlgari",
        category: "For Her",
        description: "Floral Woody Musk fragrance for women. Omnia Amethyste was launched in 2006. The nose behind this fragrance is Alberto Morillas. Top notes are Green Notes and Pink Grapefruit; middle notes are iris and Bulgarian Rose; base notes are Heliotrope and Woodsy Notes.",
        size: "85ml",
        price: 300,
        isActive: true
    },
    {
        id:"BAMBOO",
        productName: "Bamboo",
        inspiredBy: "Fresh Bamboo Scent",
        category: "Diffuser",
        description: "Gives a fresh smell of bamboo field that calms and relaxes our senses.",
        price: 200,
        size: "8ml",
        isActive: true
    },
    {
        id:"NEWCAR",
        productName: "New Car",
        inspiredBy: "New Car Scent",
        category: "Diffuser",
        description: "Car scent of a newly acquired car that defines luxury.",
        price: 200,
        size: "8ml",
        isActive: true
    },
    {
        id:"KISSES",
        productName: "Kisses",
        inspiredBy: "Little kisses scent",
        category: "Diffuser",
        description: "A sweet smell that gives a nice ambiance while having to smell it.",
        price: 200,
        size: "8ml",
        isActive: true
    },
    {
        id:"SKITTLES",
        productName: "Skittles",
        inspiredBy: "Skittles candy scent",
        category: "Diffuser",
        description: "A smell like the rainbow colored candy with fruity flavors.",
        price: 200,
        size: "8ml",
        isActive: true
    },
    {
        id:"CHERRY",
        productName: "Oh so Cherry",
        inspiredBy: "Cherry California Scent",
        category: "Diffuser",
        description: "A sweet smell of cherry.",
        price: 200,
        size: "8ml",
        isActive: true
    },
]